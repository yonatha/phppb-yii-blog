<?php

Yii::import('zii.widgets.CPortlet');

class MostVisited extends CPortlet {

    public $title = 'Most Visited';
    public $maxViews = 10;
    public $ordem_campo = 'view';
    public $ordem_tipo = 'DESC';

    public function getMostVisited() {

        $posts = Post::model()->findAllByAttributes(
                array(
            'status' => 2,
                ), array(
            'order' => $this->ordem_campo . ' ' . $this->ordem_tipo,
            'limit' => $this->maxViews,
                )
        );

        return $posts;
    }

    protected function renderContent() {
        $this->render('mostVisited');
    }

}
<?php

// change the following paths if necessary
// diretório contendo o framework
// projeto: D:\Servidor\www\phpPB\yii-blog
// framework:   D:\Servidor\www\framework\yii\
//              D:\Servidor\www\framework\zend\

$yii=dirname(__FILE__).'/../../framework/yii/yii.php';
$config=dirname(__FILE__).'/protected/config/main.php';

// remove the following line when in production mode
// defined('YII_DEBUG') or define('YII_DEBUG',true);

require_once($yii);
Yii::createWebApplication($config)->run();
